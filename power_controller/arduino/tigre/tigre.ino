/**********************************************************
* POWER CONTROLLER FOR TIGRE (Jetson Tegra X1 cluster)
*
*Author: Pierre Neyron <pierre.neyron@imag.fr>
* Copyright: LIG 2019
**********************************************************/

/*#define DEBUG*/
#ifdef DEBUG
 #define DEBUG_PRINT(x)  Serial.print(x)
#else
 #define DEBUG_PRINT(x)
#endif

/**********************************************************
* Parameter setup 
**********************************************************/
const int FIRST_PIN = 2;
const int LAST_PIN = 11;
const int PINS = 1 + LAST_PIN - FIRST_PIN;

const char ACTION_KEYS[3][PINS] = {
  {'1','2','3','4','5','6','7','8','9','0'}, /* ON keys for each pin */ 
  {'q','w','e','r','t','y','u','i','o','p'}, /* OFF keys for each pin */
  {'a','s','d','f','g','h','j','k','l',';'}, /* RESET keys for each pin */
};

/* We use the power ON jumper wire, OFF requires a long press */
const int ACTION_DELAY[3] = {100, 7000, 1000}; /* ON, OFF, RESET delay */

/* How many HIGH/LOW swaps for the action ?
  ON requires several retries sometimes.    */
const int ACTION_SWAPS[2] = {4, 1}; /* ON, OFF swaps */

/* Action period in ms */
const int TICK_STEP = 100;

/*********************************************************/

const int ACTION_ON = 0;
const int ACTION_OFF = 1;
const int ACTION_RESET = 2;

const char* ACTION_NAME[3] = {"ON", "OFF", "RESET"};

const struct {
  int code;
  char* name;
} signals[2] = {{LOW, "LOW"},{HIGH,"HIGH"}};

struct {
  int id = 0;
  int swaps = 0;
  int delay = 0;
  int reset = -1;
} actionQueue[PINS];

void setup() {
  Serial.begin(115200);
  Serial.println("CONTROLLER:TIGRE");
  for(int a = ACTION_ON; a <= ACTION_RESET; a++) {
    if (ACTION_DELAY[a] <= 0 || ACTION_DELAY[a] % TICK_STEP != 0) {
      Serial.print("ERR:DELAY:");
      Serial.println(ACTION_NAME[a]);
      exit(1);
    }
  }
  for(int pin = FIRST_PIN; pin <= LAST_PIN; pin++) {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
  }
}

void loop() {
  DEBUG_PRINT("DEBUG:LOOP"); 
  DEBUG_PRINT("\n"); 
  getCommands();
  runQueue();
  delay(TICK_STEP);
}

void getCommands() {
  char key;
  int found;
  while (Serial.available() > 0) {
    key = Serial.read();
    found = 0;
    for (int a = ACTION_ON; a <= ACTION_RESET; a++) {
      for (int k = 0; k < sizeof(ACTION_KEYS[a]); k++) {
        if (key == ACTION_KEYS[a][k]) {
          DEBUG_PRINT("KEY:");
          DEBUG_PRINT(key);
          DEBUG_PRINT(":ACTION:");
          DEBUG_PRINT(ACTION_NAME[a]);
          DEBUG_PRINT(":PIN:");
          DEBUG_PRINT(FIRST_PIN + k);
          DEBUG_PRINT("\n");
          if (a != ACTION_RESET) {
            registerAction(FIRST_PIN + k, a, -1);
          } else {
            registerAction(FIRST_PIN + k, ACTION_OFF, ACTION_DELAY[ACTION_RESET] - TICK_STEP);
          }
          found = 1;
          break;
        }
      }
      if (found > 0) {
        break;
      }
    }
    if (found > 0) {
      continue;
    }
    DEBUG_PRINT("KEY:");
    DEBUG_PRINT(key);
    DEBUG_PRINT(":ACTION:UNKNOWN");
    DEBUG_PRINT("\n");
  }
}

void registerAction(int pin, int id, int reset) {
  Serial.print("REG:");
  Serial.print(ACTION_NAME[id]);
  Serial.print(":PIN:");
  Serial.print(pin);
  int i = pin - FIRST_PIN;
  if (actionQueue[i].swaps > 0 || actionQueue[i].delay > 0 || actionQueue[i].reset >= 0) {
    Serial.println(":NA");
  } else {
    actionQueue[i].id = id;
    actionQueue[i].swaps = ACTION_SWAPS[id];
    actionQueue[i].delay = ACTION_DELAY[id];
    actionQueue[i].reset = reset;
    Serial.println(":OK");
    swapPin(pin, actionQueue[i].swaps);
  }
}

void runQueue() {
  for (int i = 0; i < PINS; i++) {
    int pin = FIRST_PIN + i;
    DEBUG_PRINT("DEBUG:");
    DEBUG_PRINT(pin);
    DEBUG_PRINT(":");
    DEBUG_PRINT(ACTION_NAME[actionQueue[i].id]);
    DEBUG_PRINT(":");
    DEBUG_PRINT(actionQueue[i].swaps);
    DEBUG_PRINT(":");
    DEBUG_PRINT(actionQueue[i].delay);
    DEBUG_PRINT(":");
    DEBUG_PRINT(actionQueue[i].reset);
    DEBUG_PRINT("\n");
    if (actionQueue[i].swaps > 0) {
      if (actionQueue[i].delay > 0) {
        actionQueue[i].delay -= TICK_STEP;
      } else {  
        actionQueue[i].swaps--;
        swapPin(pin, actionQueue[i].swaps);
        if (actionQueue[i].swaps > 0) {
          actionQueue[i].delay = ACTION_DELAY[actionQueue[i].id] - TICK_STEP;
        }
      }
    } else {
      if (actionQueue[i].reset > 0) {
          actionQueue[i].reset -= TICK_STEP;
      } else if (actionQueue[i].reset == 0) {
        actionQueue[i].reset = -1;
        actionQueue[i].id = ACTION_ON;
        actionQueue[i].swaps = ACTION_SWAPS[ACTION_ON];
        actionQueue[i].delay = ACTION_DELAY[ACTION_ON] - TICK_STEP;
      }
    }
  }
}

void swapPin(int pin, int swap) {
    digitalWrite(pin, signals[swap % 2].code);
    Serial.print("EVT:PIN:");
    Serial.print(pin);
    Serial.print(":");
    Serial.print(signals[swap % 2].name);
    Serial.print(":");
    Serial.print(swap);
    Serial.print(":");
    Serial.println(millis());
}
