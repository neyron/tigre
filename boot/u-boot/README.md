This are the boot scripts for u-boot netboot of the Tegra X1.

Node identification:
--------------------

The `hostname` variable is set beforehand in the nvram variable set of each node, so that the u-boot script can display the node name.

Netboot setup:
--------------
A default boot script is stored in the eMMC storage (this is its sole use) and is responsible for the network boot.
It contains the following:
```
echo ============================
echo = Netbooting $hostname
echo ============================
sleep 3
usb start
sleep 3
dhcp $scriptaddr u-boot.scr.d/$hostname.scr
sleep 3
source $scriptaddr
```

Remarks:
  * The `sleep` commands are workarounds for the board.
  * The `usb start` command is required because the NIC is on top of the USB stack.
  * We use the node identifier as the config filename, instead of the PXE-style filename built on the IP address of the node in in hexadecimal. This is more convenient.
  * We could write the script in the nvram instead of storing it the eMMC. Using the eMMC minimizes the changes required in the default board boot mechanism. Not using the eMMC would break the dependency on it.
  * U-boot provides a PXE emulation and UEFI chainload, which we not use, because the basic netboot function of u-boot are just sufficient.

Boot scripts descriptions:
--------------------------
  * `noop.scr.in` just show the hostname.
  * `netboot.scr.in` boots the kadeploy deploy kernel (debian buster system on ramdisk), provided by the Grid'5000 infrastructure.
  * `hdd-debian10.scr.in` boots Debian 10 on HDD, if installed.

The mkimage-tigre.sh script generates the actual `.scr` boot scripts from the `.scr.in` files.

Those files are to be served from the TFTP server, from the `u-boot.scr.d/` directory.
