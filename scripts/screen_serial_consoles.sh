#!/bin/bash

SCREENRC=$(mktemp)
cat <<EOF > $SCREENRC
defscrollback 2000
caption always "%H %0c%{=b kW}  %l  %{=r kd}%-Lw%{= bd}%50>%n%f* %t%?(%u)%?%{-}%+Lw%<%{- Wk}"
activity "Activity in %t(%n)"
# alt up/down -> focus up/down
bindkey ^[[1;3A focus up
bindkey ^[[1;3B focus down
# alt left/right -> left/right screen
bindkey ^[[1;3C next
bindkey ^[[1;3D prev

vbell on
autodetach on
startup_message off

silencewait 15
msgwait 15
EOF
for d in /dev/ttyUSB{0..8}; do
	echo "screen -t ${d#/dev/} $d 115200" >> $SCREENRC
done

for i in {0..7}; do
	echo "split" >> $SCREENRC
done

for i in {0..8}; do
	echo "select $i" >> $SCREENRC
	echo "focus down" >> $SCREENRC
done
echo "focus top" >> $SCREENRC
screen -c $SCREENRC -S tigreconsole.$JOBID
rm $SCREENRC
