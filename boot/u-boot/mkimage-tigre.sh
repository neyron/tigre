#!/bin/bash
bootfile=${1:-tx1}
bootfile=${bootfile%.in}
bootfile=${bootfile%.scr}
cat <<EOF
####################################################
### Generating $bootfile.scr from $bootfile.scr.in
####################################################
EOF
mkimage -T script -A arm64 -d $bootfile.scr.in $bootfile.scr
